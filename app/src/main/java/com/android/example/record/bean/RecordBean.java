package com.android.example.record.bean;

import org.litepal.annotation.Column;
import org.litepal.crud.LitePalSupport;

public class RecordBean extends LitePalSupport {
    @Column(unique = true)
    private int id;

    @Column(nullable = false)
    private String number;

    @Column(nullable = false)
    private String siren;

    @Column(nullable = false)
    private String record_name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public String getSiren() {
        return siren;
    }

    public void setSiren(String siren) {
        this.siren = siren;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getRecord_name() {
        return record_name;
    }

    public void setRecord_name(String record_name) {
        this.record_name = record_name;
    }
}
