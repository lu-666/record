package com.android.example.record.bean;

import android.text.TextUtils;
import android.util.Base64;

import androidx.appcompat.app.AppCompatActivity;

import com.android.example.record.utils.Utils;

import org.litepal.annotation.Column;
import org.litepal.crud.LitePalSupport;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class HistoryData extends LitePalSupport {

    @Column(unique = true)
    private int id;

    private String save_date;

    private String save_week;

    private String save_time;

    @Column(nullable = false)
    private String number;

    @Column(nullable = false, defaultValue = "0")
    private int carType;

    private String color;

    private String latitude;

    private String longitude;

    @Column(nullable = false)
    private String address;

    private String ErailName;  // 违章区域名称

    @Column(nullable = false)
    private String siren;

    @Column(defaultValue = "扣3分并罚款200元")
    private String penaltyType;

    @Column(defaultValue = "长江西路")
    String road_name;

    @Column(defaultValue = "002356")
    private String road_code;

    @Column(defaultValue = "长江西路路段")
    private String road_batch_name;

    @Column(defaultValue = "00235687")
    private String road_batch_code;

    @Column(nullable = false)
    private String img_front_uri;

    @Column(nullable = false)
    private String img_overall_uri;

    @Column(nullable = false)
    private String img_rear_uri;

    @Column(nullable = false)
    private String img_inside_uri;

    public HistoryData() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSave_date() {
        return save_date;
    }

    public void setSave_date(String save_date) {
        this.save_date = save_date;
    }

    public String getSave_week() {
        return save_week;
    }

    public void setSave_week(String save_week) {
        this.save_week = save_week;
    }

    public String getSave_time() {
        return save_time;
    }

    public void setSave_time(String save_time) {
        this.save_time = save_time;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getCarType() {
        return carType;
    }

    public void setCarType(int carType) {
        this.carType = carType;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude.substring(0, 9);
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getErailName() {
        return ErailName;
    }

    public void setErailName(String ErailName) {
        this.ErailName = ErailName;
    }

    public String getSiren() {
        return siren;
    }

    public void setSiren(String siren) {
        this.siren = siren;
    }

    public String getPenaltyType() {
        return penaltyType;
    }

    public void setPenaltyType(String penaltyType) {
        this.penaltyType = penaltyType;
    }

    public String getRoad_name() {
        return road_name;
    }

    public void setRoad_name(String road_name) {
        this.road_name = road_name;
    }

    public String getRoad_code() {
        return road_code;
    }

    public void setRoad_code(String road_code) {
        this.road_code = road_code;
    }

    public String getRoad_batch_name() {
        return road_batch_name;
    }

    public void setRoad_batch_name(String road_batch_name) {
        this.road_batch_name = road_batch_name;
    }

    public String getRoad_batch_code() {
        return road_batch_code;
    }

    public void setRoad_batch_code(String road_batch_code) {
        this.road_batch_code = road_batch_code;
    }

    public String getImg_front_uri() {
        return img_front_uri;
    }

    public void setImg_front_uri(String img_front_uri) {
        this.img_front_uri = img_front_uri;
    }

    public String getImg_overall_uri() {
        return img_overall_uri;
    }

    public void setImg_overall_uri(String img_overall_uri) {
        this.img_overall_uri = img_overall_uri;
    }

    public String getImg_rear_uri() {
        return img_rear_uri;
    }

    public void setImg_rear_uri(String img_rear_uri) {
        this.img_rear_uri = img_rear_uri;
    }

    public String getImg_inside_uri() {
        return img_inside_uri;
    }

    public void setImg_inside_uri(String img_inside_uri) {
        this.img_inside_uri = img_inside_uri;
    }

    public String jsonString(AppCompatActivity activity) {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault());
        String time = getSave_date().replace("-", "") + getSave_time().replace(":", "");
        String json;
        try {
            json =  "{" +
            "\"request\": {"+
                "\"action\": \"SetIllegalCaptureInfo\"," +
                    "\"content\": {" +
                        "\"Time\":" + "\"" + format.parse(time).getTime() / 1000 + "\"," +
                        "\"PlateNo\":" + "\"" + getNumber() + "\"," +
                        "\"CarType\": \"0\"," +
                        "\"Latitude\":" + "\"" + getLatitude() + "\"," +
                        "\"Longitude\":" + "\"" + getLongitude() + "\"," +
                        "\"Address\":" + "\"" + getAddress().replace("\n", "-") + "\"," +
                        "\"ErailName\":" + "\"" + getErailName() + "\"," +
                        "\"PoliceID\":" + "\"" + getSiren() + "\"," +
                        "\"PenaltyType\": \"扣3分并罚款200元\"," +
                        "\"roadname\": \"长江西路\"," +
                        "\"roadcode\": \"002356\"," +
                        "\"roadbatchname\": \"长江西路路段\"," +
                        "\"roadbatchcode\": \"00235687\"," +
                        "\"FileList\": [" +
                            "{" +
                                "\"ImageName\":" + "\"" + getImg_front_uri() + "\"," +
                                "\"FileSize\":" + "\"" + Utils.getFileSize(Utils.getImgPath(activity) + getImg_front_uri()) + "\"," +
                                "\"Time\":" + "\"" + format.parse(getImg_front_uri().substring(0, getImg_front_uri().lastIndexOf("."))).getTime() / 1000 + "\"," +
                                "\"FileType\":" + "\"1\"," +
                                "\"FileData\":" + "\"" + imageToBase64(getImg_front_uri(), activity) + "\"" +
                            "}," +
                            "{" +
                                "\"ImageName\":" + "\"" + getImg_overall_uri() + "\"," +
                                "\"FileSize\":" + "\"" + Utils.getFileSize(Utils.getImgPath(activity) + getImg_overall_uri()) + "\"," +
                                "\"Time\":" + "\"" + format.parse(getImg_overall_uri().substring(0, getImg_overall_uri().lastIndexOf("."))).getTime() / 1000 + "\"," +
                                "\"FileType\":" + "\"1\"," +
                                "\"FileData\":" + "\"" + imageToBase64(getImg_overall_uri(), activity) + "\"" +
                            "}," +
                            "{" +
                                "\"ImageName\":" + "\"" + getImg_rear_uri() + "\"," +
                                "\"FileSize\":" + "\"" + Utils.getFileSize(Utils.getImgPath(activity) + getImg_rear_uri()) + "\"," +
                                "\"Time\":" + "\"" + format.parse(getImg_rear_uri().substring(0, getImg_rear_uri().lastIndexOf("."))).getTime() / 1000 + "\"," +
                                "\"FileType\":" + "\"1\"," +
                                "\"FileData\":" + "\"" + imageToBase64(getImg_rear_uri(), activity) + "\"" +
                            "}," +
                            "{" +
                                "\"ImageName\":" + "\"" + getImg_inside_uri() + "\"," +
                                "\"FileSize\":" + "\"" + Utils.getFileSize(Utils.getImgPath(activity) + getImg_inside_uri()) + "\"," +
                                "\"Time\":" + "\"" + format.parse(getImg_inside_uri().substring(0, getImg_inside_uri().lastIndexOf("."))).getTime() / 1000 + "\"," +
                                "\"FileType\":" + "\"1\"," +
                                "\"FileData\":" + "\"" + imageToBase64(getImg_inside_uri(), activity)+ "\"" +
                            "}" +
                        "]" +
                    "}" +
                "}" +
            "}";
            return json;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "";
    }

    /**
     * 将图片转换成Base64编码的字符串
     */
    public static String imageToBase64(String filename, AppCompatActivity activity) {
        String path = Utils.getImgPath(activity) + filename;
        if (TextUtils.isEmpty(path)) {
            return null;
        }
        InputStream input = null;
        byte[] data;
        String base64 = null;
        try {
            input = new FileInputStream(path);
            // 创建一个字符流大小的数组。
            data = new byte[input.available()];
            // 写入数组
            input.read(data);
            // 用默认的编码格式进行编码
            base64 = Base64.encodeToString(data, Base64.NO_WRAP|Base64.NO_CLOSE|Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != input) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
        return "data:image/jpeg;base64," + base64;
    }
}