package com.android.example.record.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.android.example.record.MainActivity;
import com.android.example.record.R;
import com.android.example.record.activity.HistoryDataActivity;
import com.android.example.record.activity.QueryRecordActivity;
import com.android.example.record.adapter.ViewPager2Adapter;
import com.android.example.record.databinding.FragmentSettingBinding;
import com.android.example.record.databinding.ToolbarBinding;
import com.android.example.record.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class SettingFragment extends Fragment implements View.OnClickListener {

    public static FragmentSettingBinding binding;
    private ToolbarBinding toolbarBinding;

    private MainActivity activity;
    private List<Fragment> fragments;

    Intent intent;
    Toast toast;

    public SettingFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentSettingBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        toolbarBinding = binding.toolbar;
        initView();

        toolbarBinding.hide.setOnClickListener(this);
        binding.setSiren.setOnClickListener(this);
        binding.setUri.setOnClickListener(this);
        binding.upPloy.setOnClickListener(this);
        binding.historyData.setOnClickListener(this);
        binding.queryRecord.setOnClickListener(this);

        return view;
    }

    private void initView() {
        toolbarBinding.title.setText("设置");
        toolbarBinding.sure.setVisibility(View.GONE);

        binding.settingsDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        binding.settingsViewPager2.setUserInputEnabled(false);

        initFragment();
        ViewPager2Adapter viewPager2Adapter = new ViewPager2Adapter(activity, fragments);
        binding.settingsViewPager2.setAdapter(viewPager2Adapter);
    }

    private void initFragment() {
        fragments = new ArrayList<>();
        fragments.add(new SetSirenFragment());
        fragments.add(new SetUploadFragment());
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.hide) {
            activity.closeDrawer();
        } else if (id == R.id.set_siren) {
            binding.settingsDrawerLayout.openDrawer(GravityCompat.END);
            binding.settingsViewPager2.setCurrentItem(0);
        } else if (id == R.id.set_uri) {
            binding.settingsDrawerLayout.openDrawer(GravityCompat.END);
            binding.settingsViewPager2.setCurrentItem(1);
        } else if (id == R.id.up_ploy) {
            Utils.displayToast(getContext(), "上传策略成功", toast);
        } else if (id == R.id.history_data) {
            intent = new Intent(getActivity(), HistoryDataActivity.class);
            startActivity(intent);
        } else if (id == R.id.query_record) {
            intent = new Intent(getActivity(), QueryRecordActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof MainActivity) {
            activity = (MainActivity) context;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}