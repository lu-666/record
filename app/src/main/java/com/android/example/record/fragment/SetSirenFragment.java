package com.android.example.record.fragment;

import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.android.example.record.databinding.FragmentSetSirenBinding;
import com.android.example.record.databinding.FragmentSettingBinding;
import com.android.example.record.databinding.ToolbarBinding;
import com.android.example.record.utils.SharedPreferencesUtil;
import com.android.example.record.utils.Utils;
import com.android.example.record.viewmodel.SirenViewModel;

public class SetSirenFragment extends Fragment {

    private FragmentSettingBinding fragmentSettingBinding;
    private FragmentSetSirenBinding binding;

    Toast toast;

    public SetSirenFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        String preferences_siren = SharedPreferencesUtil.getParam(requireActivity(), SharedPreferencesUtil.SIREN, "");

        binding = FragmentSetSirenBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        ToolbarBinding toolbarBinding = binding.toolbar;

        toolbarBinding.title.setText("警号");

        binding.inputSiren.setRawInputType(InputType.TYPE_CLASS_NUMBER);
        binding.inputSiren.setText(preferences_siren);

        toolbarBinding.sure.setOnClickListener(this::setSiren);

        toolbarBinding.hide.setOnClickListener(this::closeDrawer);

        return view;
    }

    private void setSiren(View view) {
        Editable siren = binding.inputSiren.getText();
        if (siren != null && siren.length() == 6) {
            SharedPreferencesUtil.setParam(requireActivity(), SharedPreferencesUtil.SIREN, siren.toString());
            SirenViewModel viewModel = new ViewModelProvider(requireActivity()).get(SirenViewModel.class);
            viewModel.getSiren().setValue(siren.toString());
            fragmentSettingBinding.settingsDrawerLayout.closeDrawer(fragmentSettingBinding.settingsViewPager2);
        } else {
            Utils.displayToast(getContext(), "警号设置不符合要求", toast, Gravity.CENTER);
        }
    }

    private void closeDrawer(View view) {
        fragmentSettingBinding.settingsDrawerLayout.closeDrawer(fragmentSettingBinding.settingsViewPager2);
    }

    @Override
    public void onStart() {
        super.onStart();
        fragmentSettingBinding = SettingFragment.binding;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}