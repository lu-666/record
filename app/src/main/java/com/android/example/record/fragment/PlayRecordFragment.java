package com.android.example.record.fragment;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.android.example.record.R;
import com.android.example.record.activity.QueryRecordActivity;
import com.android.example.record.bean.RecordBean;
import com.android.example.record.databinding.FragmentPlayRecordBinding;
import com.android.example.record.utils.Utils;
import com.android.example.record.viewmodel.RecordIDViewModel;
import com.bumptech.glide.Glide;

import org.litepal.LitePal;

import java.io.File;

public class PlayRecordFragment extends Fragment implements MediaPlayer.OnCompletionListener {

    private FragmentPlayRecordBinding binding;

    QueryRecordActivity activity;
    private int record_id;
    RecordBean recordBean;

    public PlayRecordFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        View decorView = activity.getWindow().getDecorView();
        decorView.setSystemUiVisibility(uiOptions);
        activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        binding = FragmentPlayRecordBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        binding.play.setAlpha(0.6f);
        Glide.with(view).load(R.drawable.play).override(256, 256).into(binding.play);
        Glide.with(view).load(R.drawable.back).override(32, 32).into(binding.hide);

        binding.videoView.setOnCompletionListener(this);

        RecordIDViewModel viewModel = new ViewModelProvider(activity).get(RecordIDViewModel.class);
        viewModel.getRecordID().observe(activity, integer -> {
            record_id = integer;
            setRecordBean(LitePal.find(RecordBean.class, record_id));
            initVideoPath(getRecordBean());
        });

        binding.playRecordLayout.setOnClickListener(this::controlPlay);
        binding.hide.setOnClickListener(this::hidePlayWindow);

        return view;
    }

    private void initVideoPath(RecordBean recordBean) {
        setFirstFrame(recordBean);
        binding.videoView.setVideoPath(getVideoPath(recordBean)); // 指定视频文件的路径
        binding.videoView.pause();
        binding.play.setVisibility(View.VISIBLE);
        binding.firstFrame.setVisibility(View.VISIBLE);
    }

    private void setFirstFrame(RecordBean recordBean) {
        Glide.with(this).load(getVideoPath(recordBean)).into(binding.firstFrame);
    }

    private String getVideoPath(RecordBean recordBean) {
        String video_path = Utils.getRecordPath(activity);
        File file = new File(video_path, recordBean.getRecord_name());

        return file.getPath();
    }

    private void controlPlay(View view) {
        binding.firstFrame.setVisibility(View.GONE);
        if (!binding.videoView.isPlaying()) {
            binding.videoView.start();
            binding.play.setVisibility(View.GONE);
        } else {
            binding.videoView.pause();
            binding.play.setVisibility(View.VISIBLE);
        }
    }

    private void hidePlayWindow(View view) {
        System.out.println("点击");
        activity.showSelf();
        activity.hideFragmentView();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof QueryRecordActivity) {
            activity = (QueryRecordActivity) context;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding.videoView.suspend();
        binding = null;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        binding.videoView.suspend();
        initVideoPath(getRecordBean());
        binding.firstFrame.setVisibility(View.VISIBLE);
        binding.play.setVisibility(View.VISIBLE);
    }

    public RecordBean getRecordBean() {
        return recordBean;
    }

    public void setRecordBean(RecordBean recordBean) {
        this.recordBean = recordBean;
    }
}